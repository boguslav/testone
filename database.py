from .exceptions import IntegrityError, TableDoesNotExist, ObjectDoesNotExist


class RoomDB:

    def __init__(self, storage):
        self._storage = storage
    
    @property
    def storage(self):
        return self._storage

    def put(self, table, identifier, obj):
        if not self._in_tables(table):
            self.create_table(table)

        self.storage.put(table, identifier, obj)

        return obj
    
    def get(self, table, identifier):
        try:
            return self.storage.get(table, identifier)
        except:
            return None
    
    def all(self, table):
        if self._in_tables(table):
            return self.storage.get_table(table)
        
        raise TableDoesNotExist

    def update(self, table, identifier, obj):
        if self._in_tables(table):
            try:
                self.storage.update(table, identifier, obj)
                return self.storage.get(table, identifier)
            except:
                raise ObjectDoesNotExist

        raise TableDoesNotExist

    def create_table(self, table):
        if self._in_tables(table):
            raise IntegrityError

        self.storage.create_table(table)
        return table

    def _in_tables(self, table):
        return self.storage.in_tables(table)
