from abc import ABC
import json
import os
import shutil
import re

OPENING_LINE = r'^{\n$'
CLOSING_LINE = '}'
TABLE_OPENING_LINE = r'^\t"\w+": {\n$'
LAST_TABLE_CLOSING_LINE = '\t}\n'
NOT_LAST_TABLE_CLOSING_LINE = '\t},\n'

class Storage(ABC):
    def __init__(self, filename):
        self.tmp_name = '.roomdbtmp'
        self.filename = filename
        self.init_file()

    @property
    def write_stream(self):
        return open(self.tmp_name, 'w')

    @property
    def create_stream(self):
        return open(self.filename, 'x')

    @property
    def read_stream(self):
        return open(self.filename, 'r')

    def init_file(self):
        try:
            with self.create_stream as c:
                c.write('{\n}')
        except FileExistsError:
            pass
    
    def create_table(self, table):
        with self.write_stream as w, self.read_stream as r:
            for line in r:
                if line == LAST_TABLE_CLOSING_LINE:
                    w.write(NOT_LAST_TABLE_CLOSING_LINE)
                    continue
                if line == CLOSING_LINE:
                    w.write('\t"%s": {\n\t}\n}' % table)
                    break
 
                w.write(line)

        os.remove(self.filename)
        shutil.move(self.tmp_name, self.filename)

    def put(self, table, identifier, obj):
        with self.write_stream as w, self.read_stream as r:
            in_table = False
            added = False
            for line in r:
                if line == '\t"%s": {\n' % table:
                    in_table = True
                if in_table and '\t\t"%s": {' % identifier in line:
                    last_char = line[-1]
                    line = line[:line.find('{')] + json.dumps(obj)
                    if last_char == ',':
                        line += last_char
                    line += '\n'
                    addded = True


                if not added and in_table and line in [LAST_TABLE_CLOSING_LINE, NOT_LAST_TABLE_CLOSING_LINE]:
                    line = ('\t\t"%s": ' % identifier) + json.dumps(obj) + '\n' + line
                    in_table = False
                w.write(line)

        os.remove(self.filename)
        shutil.move(self.tmp_name, self.filename)

    def in_tables(self, table):
        with self.read_stream as r:
            for line in r:
                if line == '\t"%s": {\n' % table:
                    return True
        return False