

class RoomDBError(Exception):
    pass


class IntegrityError(RoomDBError):
    pass


class TableDoesNotExist(RoomDBError):
    pass


class ObjectDoesNotExist(RoomDBError):
    pass